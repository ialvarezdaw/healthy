import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'workout'
})
export class WorkoutPipe implements PipeTransform {
  transform(value: any) {
    if (value.type === 'endurance') {
      return ` Distancia: ${value.endurance.distance + 'km'}, Duracion: ${value.endurance.duration + 'mins'}`;
    } else {
      return `Peso: ${value.strength.weight + 'kg'}, Repeticiones: ${value.strength.reps}, Series: ${value.strength.sets}`;
    }
  }
}