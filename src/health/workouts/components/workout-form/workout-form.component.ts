import { Component, OnChanges, SimpleChanges, Input, Output, EventEmitter, ChangeDetectionStrategy } from '@angular/core';
import { FormArray, FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';

import { Workout } from '../../../shared/services/workouts/workouts.service';

@Component({
  selector: 'workout-form',
  changeDetection: ChangeDetectionStrategy.OnPush,
  styleUrls: ['workout-form.component.scss'],
  template: `
    <div class="workout-form">
      
      <form [formGroup]="form">

        <div class="workout-form__name">
          <label>
            <h3>Nombre Rutina</h3>
            <input 
              type="text" 
              [placeholder]="placeholder" 
              formControlName="name">
            <div class="error" *ngIf="required">
              Nombre de rutina es requerido.
            </div>
          </label>
          <label>
            <h3>Tipo</h3>
            <workout-type 
              formControlName="type">
            </workout-type>
          </label>
        </div>

        <div class="workout-form__details">

          <div *ngIf="form.get('type').value === 'fuerza'">
            <div
              class="workout-form__fields"
              formGroupName="strength">
              <label>
                <h3>Repeticiones</h3>
                <input type="number" formControlName="reps">
              </label>
              <label>
                <h3>Series</h3>
                <input type="number" formControlName="sets">
              </label>
              <label>
                <h3>Peso</h3>
                <input type="number" formControlName="weight">
              </label>
            </div>
          </div>

          <div *ngIf="form.get('type').value === 'físico'">
            <div
              class="workout-form__fields"
              formGroupName="endurance">
              <label>
                <h3>Distancia</h3>
                <input type="number" formControlName="distance">
              </label>
              <label>
                <h3>Duración</h3>
                <input type="number" formControlName="duration">
              </label>
            </div>
          </div>

        </div>

        <div class="workout-form__submit">
          <div>
            <button
              type="button"
              class="button"
              *ngIf="!exists"
              (click)="createWorkout()">
              Crear rutina
            </button>
            <button
              type="button"
              class="button"
              *ngIf="exists"
              (click)="updateWorkout()">
              Salvar
            </button>
            <a 
              class="button button--cancel"
              [routerLink]="['../']">
              Cancelar
            </a>
          </div>

          <div class="workout-form__delete" *ngIf="exists">
            <div *ngIf="toggled">
              <p>Borrar?</p>
              <button 
                class="confirm"
                type="button"
                (click)="removeWorkout()">
                Si
              </button>
              <button 
                class="cancel"
                type="button"
                (click)="toggle()">
                No
              </button>
            </div>

            <button class="button button--delete" type="button" (click)="toggle()">
              Borrar
            </button>
          </div>

        </div>

      </form>

    </div>
  `
})
export class WorkoutFormComponent implements OnChanges {

  toggled = false;
  exists = false;

  @Input()
  workout: Workout;

  @Output()
  create = new EventEmitter<Workout>();

  @Output()
  update = new EventEmitter<Workout>();

  @Output()
  remove = new EventEmitter<Workout>();

  form = this.fb.group({
    name: ['', Validators.required],
    type: 'fuerza',
    strength: this.fb.group({
      reps: 0,
      sets: 0,
      weight: 0
    }),
    endurance: this.fb.group({
      distance: 0,
      duration: 0
    })
  });
  
  constructor(
    private fb: FormBuilder
  ) {}

  get placeholder() {
    return `Ej: . ${this.form.get('type').value === 'fuerza' ? 'Tren inferior' : 'Elíptica'}`;
  }

  ngOnChanges(changes: SimpleChanges) {
    if (this.workout && this.workout.name) {
      this.exists = true;
      const value = this.workout;
      this.form.patchValue(value);
    }
  }

  get required() {
    return (
      this.form.get('name').hasError('required') &&
      this.form.get('name').touched
    );
  }

  createWorkout() {
    if (this.form.valid) {
      this.create.emit(this.form.value);
    }
  }

  updateWorkout() {
    if (this.form.valid) {
      this.update.emit(this.form.value);
    }
  }

  removeWorkout() {
    this.remove.emit(this.form.value);
  }

  toggle() {
    this.toggled = !this.toggled;
  }

}