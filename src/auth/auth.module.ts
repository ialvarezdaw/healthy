import { SharedModule } from './shared/shared.module';
import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { RouterModule, Routes } from "@angular/router";

// third -party modules
import { AngularFireModule, FirebaseAppConfig } from 'angularfire2';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { AngularFireDatabaseModule } from 'angularfire2/database';

export const ROUTES: Routes = [
  {
    path: 'auth',
    children: [
      { path: '', pathMatch: 'full', redirectTo: 'login'},
      { path: 'login', loadChildren: './login/login.module#LoginModule'},
      { path: 'register', loadChildren: './register/register.module#RegisterModule'},
    ]
  }
];

export const firebaseConfig: FirebaseAppConfig = {
   apiKey: "AIzaSyDMmg7Czs2JwZcSedfGQEn5B-wuPbVtooc",
   authDomain: "fitness-fcbd2.firebaseapp.com",
   databaseURL: "https://fitness-fcbd2.firebaseio.com",
   projectId: "fitness-fcbd2",
   storageBucket: "fitness-fcbd2.appspot.com",
   messagingSenderId: "353661936688"
};

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(ROUTES),
    AngularFireModule.initializeApp(firebaseConfig),
    AngularFireAuthModule,
    AngularFireDatabaseModule,
    SharedModule.forRoot()
  ]
})
export class AuthModule {}
