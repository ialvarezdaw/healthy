import { Router } from '@angular/router';
import { AuthService } from '../../shared/services/auth/auth.service';
import { FormGroup } from '@angular/forms';
import { Component } from '@angular/core';

@Component({
    selector: 'login',
    template: `
        <div>
            <auth-form (submitted)="loginUser($event)">
                <h1>Acceso</h1>
                <a routerLink="/auth/register">¿Aún no tienes cuenta? Regístrate</a>
                <button type="submit">
                    Acceso
                </button>
                <div class="error" *ngIf="error">
                    {{ error }}
                </div>
            </auth-form>
        </div>
    `
})
export class LoginComponent {

    error: string;

    constructor(
        private authService: AuthService,
        private router: Router
    ) {}

    async loginUser(event: FormGroup) {
        const { email, password } = event.value;
        try {
            await this.authService.loginUser(email, password);
            this.router.navigate(['/']);
        }
        catch (err) {
            this.error = err.message;
        }
    }

}