
[Setup and install](#setup-and-install) |
[Tasks](#tasks) |
[Resources](#resources)

## Setup and install

Fork this repo from inside GitHub so you can commit directly to your account, or simply download the `.zip` bundle with the contents inside.

#### Dependency installation

During the time building this project, you'll need development dependencies of which run on Node.js, follow the steps below for setting everything up (if you have some of these already, skip to the next step where appropriate):

1. Download and install [Node.js here](https://nodejs.org/en/download/) for Windows or for Mac.
2. Install Firebase CLI on the command-line with `npm install -g firebase-tools`

That's about it for tooling you'll need to run the project, let's move onto the project install.

#### Project installation and server

Now you've pulled down the repo and have everything setup, using the terminal you'll need to `cd` into the directory that you cloned the repo into and run some quick tasks:

```
cd <angular-pro-app>
yarn install
# OR
npm install
```

This will then setup all the development and production dependencies we need.

Now simply run this to boot up the server:

```
yarn start
# OR
npm start
```

## Tasks

A quick reminder of all tasks available:

#### Development server

```
yarn start
# OR
npm start
```

#### Production build (compile AoT)

```
yarn build:production
# OR
npm run build:production
```

#### Deploying to Firebase
You'll need to ensure you're logged into Firebase first (if you are prompted, otherwise skip to next step):

```
firebase login
```

To deploy (after running build task):

```
firebase deploy
```
## Resources

There are several resources used inside this project, of which you can read further about to dive deeper or understand in more detail what they are:

* [Angular](https://angular.io)
* [Firebase Docs](https://firebase.google.com/docs/)
* [Firebase CLI](https://firebase.google.com/docs/cli/)
* [AngularFire2 Repo/Docs](https://github.com/angular/angularfire2)
* [npm](https://www.npmjs.com/)
* [Webpack](https://webpack.js.org/)